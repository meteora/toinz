﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ToinzNPC : NPCBase
{
    List<int> list_icons = new List<int>();

    protected override void OKEvent()
    {
        InputRandomIcon();

        Debug.Log("OKEvent 실행");

        base.OKEvent();
    }

    protected override void CancelEvent()
    {
        Debug.Log("CancelEvent 실행");
        base.CancelEvent();
    }

    public void InputRandomIcon()
    {
        VR_UINPCMessageManager.okFunc -= InputRandomIcon;

        int count = Random.Range(1, 4);
        list_icons.Clear();

        for (int i = 0, imax = count; i < imax; ++i)
        {
            list_icons.Add(Random.Range(0, VR_UIManager.instance.scr_npcMessage.list_image_Icons.Count));
        }
    }

    public override void Update()
    {
        base.Update();
    }

    public void TestEvent()
    {
        print("Testing");
    }
}