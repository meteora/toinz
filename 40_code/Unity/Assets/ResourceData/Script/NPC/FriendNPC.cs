﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(SceneLoad))]
public class FriendNPC : NPCBase
{
    public GameObject SceneChangeEffect;
    public Image WhiteImg;
    List<int> list_icons = new List<int>();
    bool isLock;

    protected override void OKEvent()
    {
        Debug.Log("OKEvent 실행");

        if (b_isTalkNPC)
        {
            VR_UINPCMessageManager.okFunc -= OKEvent;
            VR_UINPCMessageManager.cancelFunc -= CancelEvent;
        }

        isLock = true;
        b_messageShowing = false;
        VR_PlayerController.instance.enu_state = PlayerState.Town_Normal;

        SceneChangeEffect.SetActive(true);
        StartCoroutine(MoveFriendTown());
    }

    IEnumerator MoveFriendTown()
    {
        do
        {
            yield return null;
        } while (WhiteImg.color.a < 1f);
        
        SceneLoad loader = GetComponent<SceneLoad>();
        if (loader != null)
        {
            yield return new WaitForSeconds(1);
            loader.UIEvent();
        }
    }

    protected override void CancelEvent()
    {
        Debug.Log("CancelEvent 실행");

        if (b_isTalkNPC)
        {
            VR_UINPCMessageManager.okFunc -= OKEvent;
            VR_UINPCMessageManager.cancelFunc -= CancelEvent;
        }

        b_messageShowing = false;
        VR_PlayerController.instance.enu_state = PlayerState.Town_Normal;
    }

    protected override IEnumerator NPCEvnetChecker()
    {
        if (isLock) yield break;
        if (!b_messageShowing)
        {
            i_curFrameReadyToMessage = 0;
            do
            {
                yield return new WaitForEndOfFrame();
                i_curFrameReadyToMessage++;

                Vector3 lookDir = Camera.main.transform.position - transform.position;

                lookDir.y = 0f;

                transform.forward = lookDir;

                if (i_curFrameReadyToMessage > VR_UIManager.instance.i_waitTalkToNPCFrame)
                {
                    StartCoroutine(DoAnimation(AnimationType.idle));
                    b_messageShowing = true;

                    VR_PlayerController.instance.enu_state = PlayerState.Town_Talk;

                    if (b_isTalkNPC)
                    {
                        List<EmoticonType> emoticons = new List<EmoticonType>();
                        emoticons.Add(EmoticonType.house);
                        emoticons.Add(EmoticonType.no);
                        VR_UIManager.instance.scr_npcMessage.Show(emoticons);

                        VR_UINPCMessageManager.okFunc += OKEvent;
                        VR_UINPCMessageManager.cancelFunc += CancelEvent;
                    }
                    if (b_isEventNPC) EventFunc();
                }
            } while (b_isCheckOn && !b_messageShowing);
        }

        yield return null;
    }

    public override void Update()
    {
        base.Update();
    }

    public void TestEvent()
    {
        print("Testing");
    }
}