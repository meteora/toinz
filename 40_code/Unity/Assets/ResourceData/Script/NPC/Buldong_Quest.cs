﻿using UnityEngine;
using System.Collections;

public class Buldong_Quest : NPCBase
{
    public GameObject go_onlyWood;
    public GameObject go_compBuldong;

    void Awake()
    {
        b_isEmoticonNPC = false;
        b_isTalkNPC     = false;
        b_isEventNPC    = true;
    }

    public override void Update()
    {
        if (MainQuestManager.instance.enum_lastClearQuestNum >= QuestNumber.OpenBuldong)
        {
            if (!go_compBuldong.activeInHierarchy || go_onlyWood.activeInHierarchy)
            {
                go_onlyWood.SetActive(false);
                go_compBuldong.SetActive(true);
            }
        }
        else
        {
            if (!go_onlyWood.activeInHierarchy || go_compBuldong.activeInHierarchy)
            {
                go_onlyWood.SetActive(true);
                go_compBuldong.SetActive(false);
            }
        }

        base.Update();
    }

    protected override void EventFunc()
    {
        if (MainQuestManager.instance.is_curQuesting && MainQuestManager.instance.enum_curQuestNum == QuestNumber.OpenBuldong)
            MainQuestManager.instance.QuestClear(QuestNumber.OpenBuldong);

        base.EventFunc();
    }
}