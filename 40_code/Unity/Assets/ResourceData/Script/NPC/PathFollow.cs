﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFollow : MonoBehaviour
{
    public GameObject go_pathParent = null;
    public List<GameObject> list_go_pos = new List<GameObject>();

    int i_currentPathIndex = 0;

    public ToinzNPC scr_npc;

    Vector3 v3_arrivalPos;
    public float f_distance;

    Vector3 v3_dirMove;

    Vector3 v3_oringinalAngle;

    bool b_isMove = true;
    bool b_waitComp = false;

    void Awake()
    {
        if (list_go_pos.Count <= 1)
            return;

        if (scr_npc == null)
            scr_npc = GetComponent<ToinzNPC>();

        v3_oringinalAngle = transform.localEulerAngles;

        v3_arrivalPos = list_go_pos[0].transform.position;
        f_distance = Vector3.Distance(transform.localPosition, v3_arrivalPos);

        if (go_pathParent == null)
            go_pathParent = transform.FindChild("path_follow").gameObject;

        PathHelper pathHelper = new PathHelper(go_pathParent.transform.parent.name, this, gameObject);
        go_pathParent.transform.parent = PathFollowRoot.instance.transform;
    }

    void Update()
    {
        if (list_go_pos.Count <= 1)
            return;

        CheckArrivalPath();
    }

    void CheckArrivalPath()
    {
        if (f_distance >= 0.05f && !scr_npc.b_messageShowing)
        {
            transform.forward = v3_arrivalPos - transform.position;
            Move();
            return;
        }

        // stop

        if (b_isMove)
        {
            b_isMove = false;
            StartCoroutine(StopCheck());
        }
    }

    void Move()
    {
        f_distance = Vector3.Distance(transform.localPosition, v3_arrivalPos);

        transform.position += v3_dirMove;
    }

    IEnumerator StopCheck()
    {
        scr_npc.DoAnimImmeduatrly(AnimationType.idle);

        yield return new WaitForSeconds(PathFollowRoot.instance.f_waitTime);
        b_isMove = true;

        while (scr_npc.b_messageShowing)
            yield return new WaitForEndOfFrame();

        i_currentPathIndex++;
        if (i_currentPathIndex == list_go_pos.Count)
            i_currentPathIndex = 0;

        v3_arrivalPos = list_go_pos[i_currentPathIndex].transform.position;
        f_distance = Vector3.Distance(transform.localPosition, v3_arrivalPos);

        transform.forward = v3_arrivalPos - transform.position;

        v3_dirMove = Vector3.Normalize(transform.forward);
        v3_dirMove *= 0.1f * PathFollowRoot.instance.f_walkSpeed;

        scr_npc.DoAnimImmeduatrly(AnimationType.run);
    }
}