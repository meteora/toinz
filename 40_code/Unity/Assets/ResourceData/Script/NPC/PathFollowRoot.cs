﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class PathHelper
{
    public PathHelper(string _name,PathFollow _scr_follow,GameObject _go_target)
    {
        Name = _name;
        scr_follow = _scr_follow;
        go_targetObj = _go_target;

        PathFollowRoot.instance.list_pathObj.Add(this);
    }

    public string Name;
    public PathFollow scr_follow;
    public GameObject go_targetObj;
}

public class PathFollowRoot : MonoBehaviour
{
    private static PathFollowRoot _instance;
    public static PathFollowRoot instance
    {
        get
        {
            if(_instance == null)
            {
                GameObject go = GameObject.Find("path_follow_Root");
                if (go != null) _instance = go.GetComponent<PathFollowRoot>();
            }

            return _instance;
        }
    }

    public float f_walkSpeed = 2f;
    public float f_waitTime = 2f;

    public List<PathHelper> list_pathObj;
    
    //void Awake()
    //{
    //    if (PathFollowRoot.instance == null)
    //        instance = this;
    //}

    void OnDestroy()
    {
        _instance = null;
    }
}