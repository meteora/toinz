﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public class NPCBase : MonoBehaviour
{
    public NPCEventType enu_npcType;

    [Header("NPC 이모티콘 관련 (자동 할당 됨)")]

    public GameObject go_emoticonCanvas;

    public Image image_myEmoticon;

    float f_curDistance = 0;
    bool b_showEmoticon = false;
    int i_checkFrameReadyToEmoticonEnd;

    public bool b_isEmoticonNPC;
    public bool b_isTalkNPC;
    public bool b_isEventNPC;

    public EmoticonType curEmoticonType;
    public List<EmoticonType> list_messgeType;

    public Animation animation;

    public List<GameObject> list_yes_talkEffect;
    public List<GameObject> list_no_talkEffect;

    protected List<EmoticonType> list_questMsgType;
    protected Component_AssignQuest componentAssigningQuest;

    void Awake()
    {
        gameObject.tag = "RayCastEvent";
        gameObject.layer = LayerMask.NameToLayer("Blocked");

        if (animation == null)
            animation = GetComponent<Animation>();

        if (b_isEmoticonNPC)
        {
            int r_data_Emoticon = Random.Range(0, 6);

            curEmoticonType = (EmoticonType)r_data_Emoticon;

            if (go_emoticonCanvas == null)
                for (int i = 0, imax = transform.childCount; i < imax; ++i)
                    if (transform.GetChild(i).GetComponent<Canvas>() != null)
                        go_emoticonCanvas = transform.GetChild(i).gameObject;

            if (image_myEmoticon == null && go_emoticonCanvas != null)
                image_myEmoticon = go_emoticonCanvas.transform.GetChild(0).GetChild(0).GetComponent<Image>();
        }

        if (b_isTalkNPC)
        {
            list_messgeType.Clear();
            int r_data_messageCount = Random.Range(1, 4);
            for (int i = 0, imax = r_data_messageCount; i < imax; ++i)
            {
                list_messgeType.Add((EmoticonType)Random.Range(0, System.Enum.GetValues(typeof(EmoticonType)).Length - 1));
            }
            
            if(image_myEmoticon != null) image_myEmoticon.sprite = VR_UIManager.instance.list_emoticon_datas[(int)curEmoticonType];
        }

        /* 퀘스트 이모티콘을 설정한다. */
        componentAssigningQuest = GetComponent<Component_AssignQuest>();
        if (componentAssigningQuest != null)
        {
            list_questMsgType = new List<EmoticonType>(componentAssigningQuest.Emoticons);
            if(image_myEmoticon != null) image_myEmoticon.sprite = VR_UIManager.instance.list_emoticon_datas[(int)EmoticonType.event1];
        }

        BoxCollider coll = GetComponent<BoxCollider>();
        if (!coll.enabled)
            coll.enabled = true;

        coll.isTrigger = true;

        if(b_isEmoticonNPC)
            StartCoroutine(ShowEmoticon());
    }

    public virtual void Update()
    {
        if (!b_isEmoticonNPC)
            return;

        f_curDistance = Vector3.Distance(
            VR_PlayerController.instance.transform.position,
            transform.position);

        if(f_curDistance < VR_UIManager.instance.f_NPCEmoticonCheckDistance)
        {
            b_showEmoticon = true;
        }
        else
        {
            i_checkFrameReadyToEmoticonEnd = 0;
            b_showEmoticon = false;
            go_emoticonCanvas.SetActive(false);
        }
    }

    IEnumerator ShowEmoticon()
    {
        i_checkFrameReadyToEmoticonEnd = 0;

        while (true)
        {
            if (!go_emoticonCanvas.activeInHierarchy && b_showEmoticon && i_checkFrameReadyToEmoticonEnd < VR_UIManager.instance.i_EmoticonShowFrame)
                go_emoticonCanvas.SetActive(true);
            else if (go_emoticonCanvas.activeInHierarchy && i_checkFrameReadyToEmoticonEnd >= VR_UIManager.instance.i_EmoticonShowFrame)
                go_emoticonCanvas.SetActive(false);

            yield return new WaitForEndOfFrame();

            i_checkFrameReadyToEmoticonEnd++;
        }
    }

    protected bool b_isCheckOn = false;
    [HideInInspector]
    public bool b_messageShowing = false;
    protected int i_curFrameReadyToMessage;

    /// <summary>
    /// EventTrigger에 사용
    /// </summary>
    public void PointerEnter()
    {
        if (!b_isCheckOn && !b_messageShowing)
        {
            b_isCheckOn = true;
            StartCoroutine(NPCEvnetChecker());
        }
    }

    /// <summary>
    /// EventTrigger에 사용
    /// </summary>
    public void PointerExit()
    {
        b_isCheckOn = false;
    }

    protected virtual IEnumerator NPCEvnetChecker()
    {
        if(!b_messageShowing)
        {
            i_curFrameReadyToMessage = 0;
            do
            {
                yield return new WaitForEndOfFrame();
                i_curFrameReadyToMessage++;

                Vector3 lookDir = Camera.main.transform.position - transform.position;

                lookDir.y = 0f;

                transform.forward = lookDir;

                if (i_curFrameReadyToMessage > VR_UIManager.instance.i_waitTalkToNPCFrame)
                {
                    StartCoroutine(DoAnimationImmediately(AnimationType.jump));
                    StartCoroutine(DoAnimation(AnimationType.idle));
                    b_messageShowing = true;

                    VR_PlayerController.instance.enu_state = PlayerState.Town_Talk;

                    if (b_isTalkNPC)
                    {
                        if(list_questMsgType != null && list_questMsgType.Count > 0)
                            VR_UIManager.instance.scr_npcMessage.Show(list_questMsgType);
                        else VR_UIManager.instance.scr_npcMessage.Show(list_messgeType);

                        VR_UINPCMessageManager.okFunc += OKEvent;
                        VR_UINPCMessageManager.cancelFunc += CancelEvent;
                    }
                    if (b_isEventNPC)
                        EventFunc();
                }
            } while (b_isCheckOn && !b_messageShowing);

        }

        yield return null;
    }

    protected virtual void EventFunc()
    {
        b_messageShowing = false;
        VR_PlayerController.instance.enu_state = PlayerState.Town_Normal;
    }

    public void DoAnim(AnimationType _aniType)
    {
        StartCoroutine(DoAnimation(_aniType));
    }

    public void DoAnimImmeduatrly(AnimationType _aniType)
    {
        StartCoroutine(DoAnimationImmediately(_aniType));
    }

    protected IEnumerator DoAnimation(AnimationType _animType)
    {
        if (animation != null)
            if (animation.GetClip(_animType.ToString()) != null)
            {
                while (!animation.IsPlaying("idle") && !animation.IsPlaying("run"))
                {
                    if (!animation.isPlaying)
                        break;
                    yield return new WaitForEndOfFrame();
                }
                animation.Play(_animType.ToString());
            }

        yield return null;
    }

    IEnumerator DoAnimationImmediately(AnimationType _animType)
    {
        if(animation != null)
            if (animation.GetClip(_animType.ToString()) != null)
            {
                animation.Stop();
                animation.Play(_animType.ToString());
            }

        yield return null;
    }

    public float f_talkFeectEffectShowingTime = 0.5f;

    IEnumerator TalkFeedbackEffcect(bool _isYes)
    {
        for (int i = 0, imax = list_yes_talkEffect.Count; i < imax; ++i)
            list_yes_talkEffect[i].SetActive(false);

        for (int i = 0, imax = list_no_talkEffect.Count; i < imax; ++i)
            list_no_talkEffect[i].SetActive(false);

        yield return new WaitForSeconds(0.5f);

        int i_randomData = 0;

        if (_isYes)
        {
            i_randomData = Random.Range(0, list_yes_talkEffect.Count);
            list_yes_talkEffect[i_randomData].SetActive(true);
        }
        else
        {
            i_randomData = Random.Range(0, list_no_talkEffect.Count);
            list_no_talkEffect[i_randomData].SetActive(true);
        }
        
        yield return new WaitForSeconds(f_talkFeectEffectShowingTime);

        if (_isYes)
            list_yes_talkEffect[i_randomData].SetActive(false);
        else
            list_no_talkEffect[i_randomData].SetActive(false);

    }

    /// <summary>
    /// NPC의 질문에 YES로 응답하는 케이스
    /// </summary>
    virtual protected void OKEvent()
    {
        if (b_isTalkNPC)
        {
            VR_UINPCMessageManager.okFunc -= OKEvent;
            VR_UINPCMessageManager.cancelFunc -= CancelEvent;
        }

        if (componentAssigningQuest != null)
        {
            componentAssigningQuest.AcceptQuest();
            if (componentAssigningQuest.IsAcceptQuest())
            {
                if (list_questMsgType != null) list_questMsgType.Clear();
                if (image_myEmoticon != null) image_myEmoticon.sprite = VR_UIManager.instance.list_emoticon_datas[(int)curEmoticonType];
            }
        }

        b_messageShowing = false;
        VR_PlayerController.instance.enu_state = PlayerState.Town_Normal;
        StartCoroutine(TalkFeedbackEffcect(true));
    }

    /// <summary>
    /// NPC의 질문에 NO로 응답하는 케이스
    /// </summary>
    virtual protected void CancelEvent()
    {
        if (b_isTalkNPC)
        {
            VR_UINPCMessageManager.okFunc -= OKEvent;
            VR_UINPCMessageManager.cancelFunc -= CancelEvent;
        }

        b_messageShowing = false;
        VR_PlayerController.instance.enu_state = PlayerState.Town_Normal;
        StartCoroutine(TalkFeedbackEffcect(false));
    }
}
