﻿using UnityEngine;
using System.Collections;

public class Gomgom_Quest : NPCBase
{
    public GameObject go_sleepEffec;
    public GameObject go_heartEffect;

    void Awake()
    {
        b_isEmoticonNPC = false;
        b_isTalkNPC     = false;
        b_isEventNPC    = true;
    }

    public override void Update()
    {
        if (MainQuestManager.instance.enum_lastClearQuestNum >= QuestNumber.WakeGomGom)
        {
            if (!go_heartEffect.activeInHierarchy || go_sleepEffec.activeInHierarchy)
            {
                go_sleepEffec.SetActive(false);
                go_heartEffect.SetActive(true);
            }
        }
        else if(MainQuestManager.instance.enum_curQuestNum == QuestNumber.WakeGomGom)
        {
            if (!go_sleepEffec.activeInHierarchy || go_heartEffect.activeInHierarchy)
            {
                go_sleepEffec.SetActive(true);
                go_heartEffect.SetActive(false);
            }
        }

        base.Update();
    }

    protected override void EventFunc()
    {
        if (MainQuestManager.instance.is_curQuesting && MainQuestManager.instance.enum_curQuestNum == QuestNumber.WakeGomGom)
            MainQuestManager.instance.QuestClear(QuestNumber.WakeGomGom);

        base.EventFunc();
    }
}