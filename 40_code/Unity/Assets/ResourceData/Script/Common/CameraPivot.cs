﻿using UnityEngine;
using System.Collections;

public class CameraPivot : MonoBehaviour
{
    public Camera camera_followRoot;

    public bool b_followX;
    public bool b_followY;
    public bool b_followZ;

    public bool b_updatePivot;

    void Awake()
    {
        if (camera_followRoot == null)
            camera_followRoot = Camera.main;

        StartCoroutine(Pivot());
    }

    public void PivotCamera(Camera _camera = null)
    {
        if (_camera != null)
            camera_followRoot = _camera;

        StartCoroutine(Pivot());
    }

    IEnumerator Pivot()
    {
        while (camera_followRoot == null || !camera_followRoot.gameObject.activeInHierarchy || !camera_followRoot.enabled)
            yield return new WaitForEndOfFrame();

        Vector3 quat_rotation = transform.eulerAngles;

        do
        {
            if (b_followX)
            {
                quat_rotation.x = camera_followRoot.transform.eulerAngles.x;
            }

            if (b_followY)
            {
                quat_rotation.y = camera_followRoot.transform.eulerAngles.y;
            }

            if (b_followZ)
            {
                quat_rotation.z = camera_followRoot.transform.eulerAngles.z;
            }

            transform.eulerAngles = quat_rotation;

            yield return new WaitForEndOfFrame();
        } while (b_updatePivot);

        yield return null;
    }
}