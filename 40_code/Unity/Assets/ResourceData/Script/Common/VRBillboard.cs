﻿using UnityEngine;
using System.Collections;

public class VRBillboard : MonoBehaviour
{
    public Camera camera_main;

    void Awake()
    {
        if (camera_main == null)
            camera_main = Camera.main;
    }

    void Update()
    {
        transform.LookAt(camera_main.transform);
    }
}
