﻿using UnityEngine;
using System.Collections;

public class UVAnimation : MonoBehaviour
{
    public UVAnimationType type_animate;
    public float f_scrolSpeed;

    public MeshRenderer meshrender;

    void Awake()
    {
        if (meshrender == null)
            meshrender = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (type_animate == UVAnimationType.None)
            return;

        float offset = Time.time * f_scrolSpeed % 1;

        switch (type_animate)
        {
            case UVAnimationType.U:
                meshrender.material.SetTextureOffset("_MainTex", new Vector2(0, offset));
                break;
            case UVAnimationType.V:
                meshrender.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
                break;
            case UVAnimationType.Both:
                meshrender.material.SetTextureOffset("_MainTex", new Vector2(offset, offset));
                break;
        }
    }
}
