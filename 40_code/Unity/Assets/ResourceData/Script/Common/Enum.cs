﻿using UnityEngine;
using System.Collections;

public enum PlayerState
{
    MyHome = 0,
    Town_Normal,
    Town_Talk
};

public enum SceneType
{
    Day,
    Night,
    MyHome,
    MyToinzTown_Day_01,
    MyToinzTown_Day_02,
    MyToinzTown_Day_03,
    MyToinzTown_Day_04,
    MyToinzTown_Day_05,
    MyToinzTown_Day_06,
    MyToinzTown_Day_07,
    MyToinzTown_Day_08,
    MyToinzTown_Day_09,
    MyToinzTown_Day_10,
    MyToinzTown_Day_11,
    MyToinzTown_Day_12,
    MyToinzTown_Day_13,
    MyToinzTown_Day_14,
    MyToinzTown_Day_15,
    MyToinzTown_Day_16,
    MyToinzTown_Day_17,
    MyToinzTown_Day_18,
    MyToinzTown_Day_19,
};

public enum NPCEventType
{
    NormalTalk,
    Quest,
    Questing,
    QuestComplete,
};

public enum UVAnimationType
{
    None,
    U,
    V,
    Both
};

/// <summary>
/// Player/MainCamera의 VR_UIManager의 list_emoticon_datas와 순서가 맞아야 한다.
/// </summary>
public enum EmoticonType
{
    anger1 = 0,
    supri1,
    sad1,
    happy1,
    love1,
    event1,
    Gomgom,
    jangjak,
    house,
    quest,
    river,
    bridge,
    no,
    teeyo,
    cat_signboard,
    farm,
    bed,
    mud,
    teeyo_black,
    teeyo_blue,
    teeyo_green,
    teeyo_red,
    teeyo_white,
};

public enum AnimationType
{
    idle = 0,
    run,
    jump
};

public enum BGMtype
{
    MyHome = 0,
    Town
};

public enum QuestNumber
{
    None = -1,
    OpenBuldong = 1,
    WakeGomGom,
    SleepInTheHome,
    BasicQuestEnd
};