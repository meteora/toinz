﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class QusetEmoticon
{
    public string name = string.Empty;
    public List<Sprite> list_image_emoticon = new List<Sprite>();
}

public class MainQuestManager : MonoBehaviour
{
    static MainQuestManager         _instance;
    public static MainQuestManager  instance
    {
        get
        {
            if (_instance != null)
                return _instance;

            return null;
        }
    }

    public List<QusetEmoticon> list_questEmoticon = new List<QusetEmoticon>();

    public QuestNumber  enum_curQuestNum        = QuestNumber.None;
    public QuestNumber  enum_lastClearQuestNum  = QuestNumber.None;
    public bool         is_curQuesting          = false;
    public bool         is_sleep = false;

    void Awake()
    {
        if (MainQuestManager.instance != null)
        {
            Destroy(gameObject);
            return;
        }

        if (MainQuestManager.instance == null)
            _instance = this;
    }

    public QuestNumber AbleQuest()
    {
        if (enum_lastClearQuestNum == QuestNumber.None)
            return (QuestNumber)1;
        else if (enum_lastClearQuestNum == QuestNumber.SleepInTheHome)
            return QuestNumber.None;
        else
            return enum_lastClearQuestNum +1;
    }

    public void QuestOn(QuestNumber _quest)
    {
        is_curQuesting      = true;
        enum_curQuestNum    = _quest;
    }

    public void QuestClear(QuestNumber _quest)
    {
        is_curQuesting          = false;
        enum_curQuestNum        = QuestNumber.None;
        enum_lastClearQuestNum  = _quest;
    }
}
