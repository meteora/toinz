﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

//[CustomEditor(typeof(ToinzNPC))]
public class ToinzNPCEditor : Editor
{
    bool b_useEmoticon = false;
    bool b_useMessage = false;

    ToinzNPC scr_npc;

    public override void OnInspectorGUI()
    {
        scr_npc = (ToinzNPC)target;

        scr_npc.enu_npcType = (NPCEventType)EditorGUILayout.EnumPopup("NPC 이벤트 타입", scr_npc.enu_npcType);

        GUILayout.Space(10);

        BoxCollider coll = scr_npc.GetComponent<BoxCollider>();
        coll.isTrigger = true;

        switch (scr_npc.enu_npcType)
        {
            case NPCEventType.NormalTalk:
                b_useMessage = true;
                UseEmoticonCheck();
                GUILayout.Space(20);
                UseMessageCheck();
                break;
            case NPCEventType.Quest:
            case NPCEventType.Questing:
            case NPCEventType.QuestComplete:
                b_useEmoticon = true;
                b_useMessage = true;
                UseEmoticonCheck();
                GUILayout.Space(20);
                UseMessageCheck();
                break;
        }

        GUILayout.Space(20);

        if(GUILayout.Button("Save"))
        {
            
        }

    }

    void UseEmoticonCheck()
    {
        b_useEmoticon = EditorGUILayout.Toggle("이모티콘 사용 여부", b_useEmoticon);

        GUILayout.Space(10);

        if (b_useEmoticon)
        {
            GUILayout.Label("이모티콘 관련 데이터");
            VR_UIManager.instance.f_NPCEmoticonCheckDistance = EditorGUILayout.FloatField("플레이어와 거리 체크 값", VR_UIManager.instance.f_NPCEmoticonCheckDistance);
        }
    }

    void UseMessageCheck()
    {
        b_useMessage = EditorGUILayout.Toggle("대화 사용 여부", b_useMessage);

        GUILayout.Space(10);

        if (b_useMessage)
        {
            GUILayout.Label("대화 관련 데이터");
            //scr_npc.i_showMessageFrameEnd = EditorGUILayout.IntField("포인터를 기다리는 시간", scr_npc.i_showMessageFrameEnd);
        }
    }
}
