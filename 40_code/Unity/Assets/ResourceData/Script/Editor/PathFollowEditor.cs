﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(PathFollow))]
public class PathFollowEditor : Editor
{
    PathFollow scr_pathFollow;

    int i_selectedIndex = -1;

    void OnSceneGUI()
    {
        scr_pathFollow = (PathFollow)target;
        
        if (scr_pathFollow.list_go_pos.Count < 1)
            return;

        for (int i = 0, imax = scr_pathFollow.list_go_pos.Count; i < imax; ++i)
        {
            Handles.color = Color.blue;

            if(i < imax -1)
                Handles.DrawLine(
                    scr_pathFollow.list_go_pos[i].transform.position,
                    scr_pathFollow.list_go_pos[i + 1].transform.position);
        }
        
        Event e = Event.current;
        int controlID = GUIUtility.GetControlID(FocusType.Passive);

        RaycastHit[] _hit;
        Ray _ray;

        switch (e.GetTypeForControl(controlID))
        {
            case EventType.MouseDown:
                Debug.Log("MouseDown : [" + controlID + "]");

                _ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

                _hit = Physics.RaycastAll(_ray);

                i_selectedIndex = -1;

                for (int i=0,imax=_hit.Length;i< imax; ++i)
                    if (_hit[i].transform.CompareTag("path_follow"))
                    {
                        i_selectedIndex = int.Parse(_hit[i].transform.name);
                        break;
                    }

                GUIUtility.hotControl = controlID;
                e.Use();
                break;
            case EventType.MouseDrag:
                Debug.Log("MouseDrag : [" + controlID + "]");

                if (i_selectedIndex == -1)
                    return;

                _ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                _hit = Physics.RaycastAll(_ray);

                for (int i = 0, imax = _hit.Length; i < imax; ++i)
                    if (_hit[i].transform.CompareTag("Ground"))
                    {
                        scr_pathFollow.list_go_pos[i_selectedIndex].transform.position =
                            _hit[i].point;
                        break;
                    }

                GUIUtility.hotControl = controlID;
                e.Use();
                break;
            case EventType.MouseUp:
                i_selectedIndex = -1;

                Debug.Log("MouseUp : [" + controlID + "]");
                GUIUtility.hotControl = 0;
                e.Use();
                break;
        }
    }

    public override void OnInspectorGUI()
    {
        scr_pathFollow = (PathFollow)target;


        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Distance : ");
        GUILayout.Space(10);
        GUILayout.Label(scr_pathFollow.f_distance.ToString());

        EditorGUILayout.EndHorizontal();

        for (int i = 0, imax = scr_pathFollow.list_go_pos.Count; i < imax; ++i)
        {
            EditorGUILayout.BeginHorizontal();

            float _x = EditorGUILayout.FloatField(scr_pathFollow.list_go_pos[i].transform.localPosition.x);
            float _y = EditorGUILayout.FloatField(scr_pathFollow.list_go_pos[i].transform.localPosition.y);
            float _z = EditorGUILayout.FloatField(scr_pathFollow.list_go_pos[i].transform.localPosition.z);

            scr_pathFollow.list_go_pos[i].transform.localPosition = new Vector3(_x, _y, _z);

            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Add Path"))
        {
            if (PathFollowRoot.instance == null)
            {
                GameObject go_pathRoot = new GameObject("path_follow_Root");
                go_pathRoot.transform.parent = null;
                go_pathRoot.transform.position = Vector3.zero;
                go_pathRoot.transform.rotation = Quaternion.identity;

                go_pathRoot.AddComponent<PathFollowRoot>();
                //PathFollowRoot.instance = go_pathRoot.GetComponent<PathFollowRoot>();

                EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene()); ;
            }

            if (scr_pathFollow.go_pathParent == null)
            {
                if (scr_pathFollow.transform.FindChild("path_follow") != null)
                    scr_pathFollow.go_pathParent = scr_pathFollow.transform.FindChild("path_follow").gameObject;
                else
                {
                    scr_pathFollow.go_pathParent = new GameObject("path_follow");

                    scr_pathFollow.go_pathParent.transform.parent = scr_pathFollow.transform;

                    scr_pathFollow.go_pathParent.transform.localPosition = Vector3.zero;
                    scr_pathFollow.go_pathParent.transform.localRotation = Quaternion.identity;
                }
            }

            GameObject go_singlePath = new GameObject(scr_pathFollow.list_go_pos.Count.ToString());

            go_singlePath.transform.parent = scr_pathFollow.go_pathParent.transform;

            if (scr_pathFollow.list_go_pos.Count == 0)
                go_singlePath.transform.localPosition = Vector3.zero;
            else
                go_singlePath.transform.localPosition = scr_pathFollow.list_go_pos[scr_pathFollow.list_go_pos.Count-1].transform.localPosition 
                    + new Vector3(0.5f,0,0);

            go_singlePath.transform.localRotation = Quaternion.identity;

            BoxCollider col_data = go_singlePath.AddComponent<BoxCollider>();
            col_data.tag = "path_follow";
            col_data.size = new Vector3(0.2f, 0.2f, 0.2f);
            col_data.center = new Vector3(0, col_data.size.y / 2f, 0);

            scr_pathFollow.list_go_pos.Add(go_singlePath);
        }

        GUILayout.Space(10);

        if (GUILayout.Button("Delete Last"))
        {
            if (scr_pathFollow.list_go_pos.Count >= 1)
            {
                GameObject go_removingData = scr_pathFollow.list_go_pos[scr_pathFollow.list_go_pos.Count - 1];
                scr_pathFollow.list_go_pos.RemoveAt(scr_pathFollow.list_go_pos.Count - 1);

                DestroyImmediate(go_removingData);
            }
        }

        if (GUILayout.Button("Delete ALL"))
        {
            if (scr_pathFollow.list_go_pos.Count >= 1)
            {
                for (int i = 0, imax = scr_pathFollow.list_go_pos.Count; i < imax; ++i)
                {
                    GameObject go_removingData = scr_pathFollow.list_go_pos[scr_pathFollow.list_go_pos.Count - 1];
                    scr_pathFollow.list_go_pos.RemoveAt(scr_pathFollow.list_go_pos.Count - 1);

                    DestroyImmediate(go_removingData);
                }
            }
        }
    }
}