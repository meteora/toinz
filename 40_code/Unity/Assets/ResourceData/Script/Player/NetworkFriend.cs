﻿using UnityEngine;
using System.Collections;

public class NetworkFriend : MonoBehaviour
{
    public string str_uid;
    Vector3 prev_pos = Vector3.zero;
    public Animation animationData;
    public void Location(Vector3 _v3_loc)
    {
        if (animationData == null)
            animationData = GetComponent<Animation>();

        // 봐야겠다 어느정도 움직이는 지
        if (Vector3.Distance(prev_pos, _v3_loc) > 0.1f)
        {
            Vector3 frontVector = _v3_loc - prev_pos;
            transform.forward = frontVector;

            animationData.Play("run");
        }
        else
            animationData.Play("idle");

        prev_pos = transform.position;
        transform.position = _v3_loc;
    }
}
