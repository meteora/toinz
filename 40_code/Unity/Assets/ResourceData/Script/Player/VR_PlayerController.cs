﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VR_PlayerController : MonoBehaviour
{
    public static VR_PlayerController instance
    {
        get
        {
            if (_instance != null)
                return _instance;

            return null;
        }
    }

    static VR_PlayerController _instance;

    public PlayerState enu_state = PlayerState.MyHome;

    public Transform tr_head;
    Quaternion qu_prev_headRotation = Quaternion.identity;

    Vector3 v3_movedir;

    public bool b_canWalk = false;

    public float f_speed = 0.01f;

    public float f_blockSpace = 0.5f;

    [Header("gvr 관련이슈 덕분에 활성화를 따로 해주기")]
    public EventSystem eventsystem;
    public GvrReticle gvrreticle;

    public List<GameObject> list_awake_activeData;

    public List<AudioClip> list_audioClip;
    public AudioSource audio_bgm;

    public AudioSource audio_walking;

    //public TestSocketIO scr_socketio;

    public GameObject go_myCharacter;
    public Animation ani_myCharacter;

    public float f_homeCameraPosY;
    public float f_outCameraPosY;

    public Image QuestIcon;

    public GameObject UIMyRoomGuide;
    public GameObject UIStartGuide;
    public GameObject UIQuestGuide;
    
    public Dictionary<string, NetworkFriend> dict_friendsData = new Dictionary<string, NetworkFriend>();

    public bool b_Stop = false; // 화면을 터치했을 때 캐릭터를 멈추게 하기 위한 변수.

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (_instance == null)
        {
            _instance = this;
            _instance.HideQuestIcon();
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        Application.targetFrameRate = 60;

        for(int i=0,imax = list_awake_activeData.Count;i< imax; ++i)
        {
            list_awake_activeData[i].SetActive(true);
        }

        if (tr_head == null)
            for (int i = 0, imax = transform.childCount; i < imax; ++i)
            {
                if (transform.GetChild(i).name == "Head")
                {
                    tr_head = transform.GetChild(i);
                    break;
                }
            }

        qu_prev_headRotation = tr_head.rotation;
        
        PlayBGM(BGMtype.MyHome);
    }

    public void PlayBGM(BGMtype _type)
    {
        if (audio_bgm == null)
            audio_bgm = GetComponent<AudioSource>();

        audio_bgm.Stop();
        audio_bgm.clip = list_audioClip[(int)_type];
        audio_bgm.loop = true;

        audio_bgm.Play();
    }


    StringBuilder stb_rotationValue = new StringBuilder();

    [SerializeField]
    int i_yesPoint;
    [SerializeField]
    int i_noPoint;

    // test
    bool b_isYes;
    void Update()
    {
        //if (!scr_socketio.isDone)
        //    return;

        //if (MainQuestManager.instance.is_sleep)
        //    return;

        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

        stb_rotationValue.Remove(0, stb_rotationValue.Length);

        stb_rotationValue.Append("x : ");
        stb_rotationValue.Append(tr_head.rotation.x);
        stb_rotationValue.Append("\n y : ");
        stb_rotationValue.Append(tr_head.rotation.y);
        stb_rotationValue.Append("\n z : ");
        stb_rotationValue.Append(tr_head.rotation.z);
        stb_rotationValue.Append("\n w : ");
        stb_rotationValue.Append(tr_head.rotation.w);

        // 터치가 있을 때 멈춤을 토글한다.
        if (Input.GetMouseButtonDown(0)) b_Stop = !b_Stop;

        if (!audio_walking.isPlaying && b_canWalk && enu_state == PlayerState.Town_Normal)
        {
            ani_myCharacter.Play("run");
            audio_walking.Play();
        }
        else if (audio_walking.isPlaying && (!b_canWalk || b_Stop))
        {
            ani_myCharacter.Play("idle");
            audio_walking.Stop();
        }

        v3_movedir = tr_head.forward;
        v3_movedir.y = 0f;
        v3_movedir = Vector3.Normalize(v3_movedir);

        if (go_myCharacter.activeInHierarchy)
            go_myCharacter.transform.forward = v3_movedir;

        if (b_canWalk && enu_state == PlayerState.Town_Normal && !b_Stop)
        {
            v3_movedir *= f_speed;

            transform.position += v3_movedir;
        }

        if (enu_state == PlayerState.Town_Talk)
        {
            float moveX = Mathf.Abs(tr_head.rotation.x) - Mathf.Abs(qu_prev_headRotation.x);
            float moveY = Mathf.Abs(tr_head.rotation.y) - Mathf.Abs(qu_prev_headRotation.y);

            float calcValue = moveX - moveY;

            if (moveX >= 0.05f || moveX <= -0.05f)
            {
                i_yesPoint++;
                qu_prev_headRotation = tr_head.rotation;
                Debug.Log("Yes:" + i_yesPoint);
            }
            else if (moveY <= -0.08f || moveY >= 0.08f)
            {
                i_noPoint++;
                qu_prev_headRotation = tr_head.rotation;
                Debug.Log("No:" + i_noPoint);
            }

            if (i_yesPoint >= 5)
            {
                i_yesPoint = 0;
                i_noPoint = 0;

                b_isYes = true;

                if(VR_UIManager.instance.scr_npcMessage != null) VR_UIManager.instance.scr_npcMessage.TalkFeedBack(b_isYes);
            }
            else if (i_noPoint >= 5)
            {
                i_yesPoint = 0;
                i_noPoint = 0;

                b_isYes = false;

                if (VR_UIManager.instance.scr_npcMessage != null) VR_UIManager.instance.scr_npcMessage.TalkFeedBack(b_isYes);
            }

            stb_rotationValue.Append("\nCalc Value : ");
            stb_rotationValue.Append(calcValue);
        }

        stb_rotationValue.Append("\nYesPoint : ");
        stb_rotationValue.Append(i_yesPoint);

        stb_rotationValue.Append("\nNoPoint : ");
        stb_rotationValue.Append(i_noPoint);

        stb_rotationValue.Append("\nResult : ");
        stb_rotationValue.Append(b_isYes.ToString());

        float fps = 1.0f / deltaTime;

        stb_rotationValue.Append("\nFrame : ");
        stb_rotationValue.Append(fps.ToString());
    }

    float deltaTime = 0.0f;

    public int i_reactWaitFrame = 180;

    NPCBase eventtrigger_npc;
    FakeEventTrigger eventtrigger_fake;

    Component_CompleteQuestArea questTrigger;

    void FixedUpdate()
    {
        //if (!scr_socketio.isDone)
        //    return;

        if (enu_state == PlayerState.Town_Talk)
            return;

        //if (MainQuestManager.instance.is_sleep)
        //    return;

        // 1인칭 3인칭 분기 나눠야함

        if (go_myCharacter.activeInHierarchy)
        {
            RaycastHit hit;

            Vector3 rayPos = go_myCharacter.transform.position;
            rayPos.y = 0.2f;

            if (Physics.Raycast(rayPos, go_myCharacter.transform.forward, out hit, f_blockSpace))
            {
                if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Blocked"))
                {
                    b_canWalk = false;
                    eventtrigger_npc = hit.transform.gameObject.GetComponent<NPCBase>();
                    eventtrigger_fake = hit.transform.gameObject.GetComponent<FakeEventTrigger>();
                    questTrigger = hit.transform.gameObject.GetComponent<Component_CompleteQuestArea>();
                    if (eventtrigger_npc != null)
                    {
                        eventtrigger_npc.PointerEnter();
                    }
                    if (eventtrigger_fake != null)
                    {
                        eventtrigger_fake.PointerEnter();
                    }
                    if (questTrigger != null)
                    {
                        if (QuestManager.CanCompletedQuestId(questTrigger.QuestId)) questTrigger.CompleteQuest();
                        //questTrigger.SendMessage("CompleteQuest", SendMessageOptions.DontRequireReceiver);
                    }
                }
                else
                {
                    if (eventtrigger_npc != null)
                    {
                        eventtrigger_npc.PointerExit();
                        eventtrigger_npc = null;
                    }

                    if (eventtrigger_fake != null)
                    {
                        eventtrigger_fake.PointerExit();
                        eventtrigger_fake = null;
                    }

                    b_canWalk = true;
                }
                    
            }
            else
            {
                b_canWalk = true;
                if (eventtrigger_npc != null)
                {
                    eventtrigger_npc.PointerExit();
                    eventtrigger_npc = null;
                }
                if (eventtrigger_fake != null)
                {
                    eventtrigger_fake.PointerExit();
                    eventtrigger_fake = null;
                }

            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other == null)
            return;
        
        if (other.CompareTag("PlayerHeightCheck"))
        {
            VR_PlayerController.instance.transform.position =
                new Vector3(VR_PlayerController.instance.transform.position.x,
                0.8f,
                VR_PlayerController.instance.transform.position.z);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other == null)
            return;

        if (other.CompareTag("PlayerHeightCheck"))
        {
            VR_PlayerController.instance.transform.position =
                new Vector3(VR_PlayerController.instance.transform.position.x,
                0.5f,
                VR_PlayerController.instance.transform.position.z);
        }
    }

    public void PlayerSleep()
    {
        StartCoroutine(SleepEffect());
    }

    public GameObject go_blackQuad;
    IEnumerator SleepEffect()
    {
        go_blackQuad.SetActive(true);

        Material material_black = go_blackQuad.GetComponent<Renderer>().material;
        Color color_black = material_black.GetColor("_Color");

        color_black.a = 1f;
        material_black.SetColor("_Color", color_black);

        for (int i=0,imax = 20;i< imax; ++i)
        {
            color_black.a -= 0.05f;
            material_black.SetColor("_Color", color_black);

            yield return new WaitForSeconds(0.05f);
        }
        
        //MainQuestManager.instance.is_sleep = false;
        go_blackQuad.SetActive(false);
    }

    public void ShowQuestIcon(EmoticonType _type)
    {
        QuestIcon.sprite = VR_UIManager.instance.list_emoticon_datas[(int)_type];
        //QuestIcon.gameObject.SetActive(true);
        QuestIcon.color = new Color(1f, 1f, 1f, 1f);
    }

    public void HideQuestIcon()
    {
        QuestIcon.color = new Color(1f, 1f, 1f, 0f);
        //QuestIcon.color.a = 0f;.gameObject.SetActive(false);
    }

    bool hasShowStartGuide = false;
    public void ShowStartGuideUI()
    {
        if (hasShowStartGuide) return;
        hasShowStartGuide = true;
        UIStartGuide.SetActive(true);
        UIStartGuide.GetComponent<Animation>().Play();
    }

    bool hasShowQuestGuide = false;
    public void ShowQuestGuideUI()
    {
        if (hasShowQuestGuide) return;
        hasShowQuestGuide = true;
        UIQuestGuide.SetActive(true);
        UIQuestGuide.GetComponent<Animation>().Play();
    }

    public void HideMyRoomGuideUI()
    {
        UIMyRoomGuide.SetActive(false);
    }
}