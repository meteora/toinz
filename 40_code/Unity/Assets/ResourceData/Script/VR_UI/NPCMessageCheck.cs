﻿using UnityEngine;
using System.Collections;

public class NPCMessageCheck : VR_UIAct
{
    public bool b_isYes;

    public override void UIEvent()
    {
        if (b_isYes)
        {
            VR_UINPCMessageManager.okFunc();
        }
        else
        {
            VR_UINPCMessageManager.cancelFunc();
        }
    }
}