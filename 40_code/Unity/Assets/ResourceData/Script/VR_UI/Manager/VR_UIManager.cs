﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class VR_UIManager : MonoBehaviour
{
    static VR_UIManager _instance;

    public static VR_UIManager instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = GameObject.Find("Player");
                if (go != null) _instance = go.GetComponentInChildren<VR_UIManager>();
            }
            return _instance;
        }
    }

    //public Text text_rotationValue;

    bool b_canUI_Evnet = true;

    public VR_UINPCMessageManager scr_npcMessage;

    [Header("NPC 이모티콘 보여지는 프레임")]
    public int i_EmoticonShowFrame = 300;

    [Header("NPC 이모티콘 보여지는 거리")]
    public float f_NPCEmoticonCheckDistance = 7f;

    [Header("NPC 대화 VR 포인트 기다리는 프레임")]
    public int i_waitTalkToNPCFrame = 180;

    [Header("이모티콘 총 집합")]
    public List<Sprite> list_emoticon_datas;

    public void VR_EventChecker(VR_UIAct _scr_ui_act)
    {
        if (b_canUI_Evnet)
        {
            b_canUI_Evnet = false;
            StartCoroutine(VR_UI_Event(_scr_ui_act));
        }
    }

    IEnumerator VR_UI_Event(VR_UIAct _scr_ui_act)
    {
        int ui_checkFrame = 0;

        do
        {
            yield return new WaitForEndOfFrame();
            ui_checkFrame++;

            if (ui_checkFrame > 90)
            {
                _scr_ui_act.UIEvent();
                b_canUI_Evnet = true;
                break;
            }

        } while (!b_canUI_Evnet);
    }


    public void VR_EventExit()
    {
        b_canUI_Evnet = true;
    }

    void FixedUpdate()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position,transform.forward,out hit, 100.0f))
        {
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("UI") && hit.transform.GetComponent<VR_UIAct>() != null)
            {
                VR_UIAct ui_act = hit.transform.GetComponent<VR_UIAct>();

                ui_act.SendMessage("UIEvent");
            }
        }
    }
}