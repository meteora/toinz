﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class VR_UINPCMessageManager : MonoBehaviour
{
    public delegate void OKDelegate();
    public static OKDelegate okFunc;

    public delegate void CancelDelegate();
    public static CancelDelegate cancelFunc;

    public Image image_messageBox;
    public List<Image> list_image_Icons;

    NPCBase scr_npc;

    public CameraPivot scr_pivot;

    /// <summary>
    /// UI로 보여줭
    /// </summary>
    /// <param name="_characterPos"></param>
    /// <param name="_list_Icons">Icon Count (Max 3)</param>
    public void Show(List<EmoticonType> _list_emoticonType_datas = null)
    {
        if (scr_npc != null)
        {
            Debug.Log("스크립트가 없네?");
            return;
        }

        if (_list_emoticonType_datas == null)
        {
            Debug.Log("이모티콘 리스트가 비어있네?");

            int randomCount = Random.Range(1, 4);

            _list_emoticonType_datas.Clear();

            for(int i=0,imax = randomCount;i< imax; ++i)
            {
                _list_emoticonType_datas.Add((EmoticonType)Random.Range(0, System.Enum.GetValues(typeof(EmoticonType)).Length - 1));
            }
        }

        if (_list_emoticonType_datas.Count > 4)
            _list_emoticonType_datas.RemoveRange(3, _list_emoticonType_datas.Count - 3);

        float xPos;
        
        switch (_list_emoticonType_datas.Count)
        {
            case 1:
                list_image_Icons[0].transform.localPosition = new Vector3(0f,0f,-0.01f);
                break;
            case 2:
                xPos = 60f;
                list_image_Icons[0].transform.localPosition = new Vector3(-xPos,0f);
                list_image_Icons[1].transform.localPosition = new Vector3(xPos, 0f);
                break;
            case 3:
                xPos = 100f;
                list_image_Icons[0].transform.localPosition = new Vector3(-xPos, 0f);
                list_image_Icons[1].transform.localPosition = new Vector3(0f, 0f);
                list_image_Icons[2].transform.localPosition = new Vector3(xPos, 0f);
                break;
        }

        for (int i = 0, imax = list_image_Icons.Count; i < imax; ++i)
            list_image_Icons[i].gameObject.SetActive(false);

        for (int i = 0, imax = _list_emoticonType_datas.Count; i < imax; ++i)
        {
            list_image_Icons[i].sprite = VR_UIManager.instance.list_emoticon_datas[(int)_list_emoticonType_datas[i]];
            list_image_Icons[i].gameObject.SetActive(true);
        }

        VR_PlayerController.instance.enu_state = PlayerState.Town_Talk;

        scr_pivot.PivotCamera();

        image_messageBox.gameObject.SetActive(true);
    }

    void QuestOK()
    {
        VR_UINPCMessageManager.okFunc -= QuestOK;
    }

    public void Hide()
    {
        image_messageBox.gameObject.SetActive(false);
    }

    public void TalkFeedBack(bool _isYes)
    {
        Hide();

        if (_isYes && okFunc != null) okFunc();
        else if (!_isYes && cancelFunc != null) cancelFunc();

        VR_PlayerController.instance.ShowQuestGuideUI();
    }
}
