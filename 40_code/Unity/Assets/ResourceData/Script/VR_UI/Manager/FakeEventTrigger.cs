﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class FakeEventTrigger : MonoBehaviour
{
    public void PointerEnter(VR_UIAct _act = null)
    {
        //if (!VR_PlayerController.instance.scr_socketio.isDone)
        //    return;

        if (_act == null)
            _act = gameObject.GetComponent<VR_UIAct>();

        VR_UIManager.instance.VR_EventChecker(_act);
    }

    public void PointerExit()
    {
        //if (!VR_PlayerController.instance.scr_socketio.isDone)
        //    return;

        VR_UIManager.instance.VR_EventExit();
    }
}
