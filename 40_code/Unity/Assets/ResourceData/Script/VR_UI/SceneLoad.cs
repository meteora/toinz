﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Text;

public class SceneLoad : VR_UIAct
{
    public SceneType enu_type;
    public FxObject SceneLoadFx;    

    bool isLock;
    public override void UIEvent()
    {
        if (isLock) return;
        isLock = true;
        if (SceneLoadFx == null) LoadScene();
        else StartCoroutine(ShowFx());
    }

    IEnumerator ShowFx()
    {
        int num = 5;
        while(num > 0)
        {
            SceneLoadFx.Show(num);
            yield return new WaitForSeconds(1f);
            num--;
        }

        LoadScene();
    }

    void LoadScene()
    {
        // Town 씬 호출
        SceneManager.LoadScene(enu_type.ToString());
        if (enu_type != SceneType.MyHome)
        {
            VR_PlayerController.instance.transform.position = new Vector3(-9f, 0.5f, -7.5f);
            VR_PlayerController.instance.b_canWalk = true;
            VR_PlayerController.instance.enu_state = PlayerState.Town_Normal;
            VR_PlayerController.instance.PlayBGM(BGMtype.Town);

            Camera.main.transform.localPosition = new Vector3(0, VR_PlayerController.instance.f_outCameraPosY, 0);
            VR_PlayerController.instance.go_myCharacter.SetActive(true);
        }
        else
        {
            VR_PlayerController.instance.PlayerSleep();
            VR_PlayerController.instance.transform.position = new Vector3(0f, 0.5f, 0f);
            VR_PlayerController.instance.b_canWalk = false;
            VR_PlayerController.instance.enu_state = PlayerState.MyHome;
            VR_PlayerController.instance.PlayBGM(BGMtype.MyHome);

            Camera.main.transform.localPosition = new Vector3(0, VR_PlayerController.instance.f_homeCameraPosY, 0);
            VR_PlayerController.instance.go_myCharacter.SetActive(false);
        }
        VR_PlayerController.instance.HideMyRoomGuideUI();
        VR_PlayerController.instance.ShowStartGuideUI();
    }
}
