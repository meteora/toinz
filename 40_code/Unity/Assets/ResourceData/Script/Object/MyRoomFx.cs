﻿using UnityEngine;
using System.Collections;

public class MyRoomFx : FxObject {

    public GameObject OpenTextObj;
    public GameObject CountdownObj;
    public Sprite[] Nums;

    private SpriteRenderer countdownSpriteRenderer;

	// Use this for initialization
	void Start () {

        if (CountdownObj != null)
        {
            countdownSpriteRenderer = CountdownObj.GetComponent<SpriteRenderer>();
            CountdownObj.SetActive(false);
        }
	
	}

    public override void Show(int _num)
    {
        try
        {
            if (!CountdownObj.activeSelf)
            {
                OpenTextObj.SetActive(false);
                CountdownObj.SetActive(true);
            }
            countdownSpriteRenderer.sprite = Nums[_num];
        }
        catch
        {
            return;
        }
    }
}
