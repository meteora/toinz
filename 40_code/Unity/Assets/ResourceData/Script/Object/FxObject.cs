﻿using UnityEngine;
using System.Collections;

public class FxObject : MonoBehaviour, IFx {

    public virtual void Show(int _num)
    {
        // 각 클래스에서 효과를 구현한다.
    }
}
