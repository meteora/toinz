﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class QuestManager : MonoBehaviour {

    public Action<int> QuestCompletedEvent;

    [SerializeField]
    private List<int> listAcceptQuestId;
    [SerializeField]
    private List<int> listCompletedQuestId;
    [SerializeField]
    private List<int> listMainQuestId;
    [SerializeField]
    private Queue<int> queueMainQuest;
    private static QuestManager instance;
    public static QuestManager Instance
    {
        get
        {
            if(instance == null)
            {
                GameObject go = GameObject.Find("Player");
                if(go != null)
                {
                    instance = go.AddComponent<QuestManager>();
                    instance.listAcceptQuestId = new List<int>();
                    instance.listCompletedQuestId = new List<int>();
                    instance.listMainQuestId = new List<int>();
                    instance.listMainQuestId.Add(1);
                    instance.listMainQuestId.Add(4);
                    instance.listMainQuestId.Add(5);
                    instance.queueMainQuest = new Queue<int>();
                    foreach(int id in instance.listMainQuestId)
                    {
                        instance.queueMainQuest.Enqueue(id);
                    }

                    VR_PlayerController.instance.HideQuestIcon();
                }
            }

            return instance;
        }
    }

    public static bool CanAcceptQuestId(int _id)
    {
        if (Instance == null) return false;
        if (Instance.listCompletedQuestId.Contains(_id)) return false;
        if (Instance.listMainQuestId.Contains(_id))
        {
            if (Instance.queueMainQuest.Peek() != _id) return false; 
        }
        return !Instance.listAcceptQuestId.Contains(_id);
    }

    public static bool CanCompletedQuestId(int _id)
    {
        if (Instance == null) return false;
        if (!Instance.listAcceptQuestId.Contains(_id)) return false;
        return !Instance.listCompletedQuestId.Contains(_id);
    }

    public static bool IsCompletedQuest(int _id)
    {
        if (Instance == null) return false;
        return Instance.listCompletedQuestId.Contains(_id);
    }

    public static void AcceptQuest(int _id, EmoticonType _icon)
    {
        if (CanAcceptQuestId(_id))
        {
            Instance.listAcceptQuestId.Add(_id);
            if (Instance.listMainQuestId.Contains(_id))
            {
                VR_PlayerController.instance.ShowQuestIcon(_icon);
            }
        }
    }

    public static bool IsAcceptQuest(int _id)
    {
        return Instance.listAcceptQuestId.Contains(_id);
    }

    public static void CompleteQuest(int _id)
    {
        if (CanCompletedQuestId(_id))
        {
            Instance.listAcceptQuestId.Remove(_id);
            Instance.listCompletedQuestId.Add(_id);
            if (Instance.QuestCompletedEvent != null) Instance.QuestCompletedEvent(_id);

            if (Instance.listMainQuestId.Contains(_id))
            {
                Instance.queueMainQuest.Dequeue();
                VR_PlayerController.instance.HideQuestIcon();
            }
        }
    }
}
