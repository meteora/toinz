﻿using UnityEngine;
using System.Collections;

public class EventListener_QuestCompleted : MonoBehaviour {

    public int QuestId;
    public string MethodName;
    public GameObject[] Targets;
    public GameObject[] HidedObjectsWhenClear;

    void Awake()
    {
        QuestManager.Instance.QuestCompletedEvent += ClearQuest;
        if (QuestId > 0 && Targets != null && !QuestManager.IsCompletedQuest(QuestId))
        {
            for (int i = 0; i < Targets.Length; ++i)
            {
                if (Targets[i] != null) Targets[i].SetActive(false);
                else Debug.LogWarning("타겟 오브젝트가 설정되지 않았습니다.");
            }
        }
        if (QuestId > 0 && HidedObjectsWhenClear != null && QuestManager.IsCompletedQuest(QuestId))
        {
            for (int i = 0; i < HidedObjectsWhenClear.Length; ++i)
            {
                if(HidedObjectsWhenClear[i] != null) HidedObjectsWhenClear[i].SetActive(false);
                else Debug.LogWarning("퀘스트 완료 시 숨겨질 오브젝트가 설정되지 않았습니다.");
            }
        }
    }

    void ClearQuest(int _id)
    {
        if (QuestId == _id && Targets != null)
        {
            for(int i=0; i < Targets.Length; ++i)
            {
                if (Targets[i] != null) Targets[i].SetActive(true);
            }
        }

        if (QuestId == _id && HidedObjectsWhenClear != null)
        {
            for (int i = 0; i < HidedObjectsWhenClear.Length; ++i)
            {
                if (HidedObjectsWhenClear[i] != null) HidedObjectsWhenClear[i].SetActive(false);
            }
        }
    }

    void OnDestroy()
    {
        QuestManager.Instance.QuestCompletedEvent -= ClearQuest;
    }
}
