﻿using UnityEngine;
using System.Collections;

public class Component_CompleteQuestArea : MonoBehaviour
{
    public int QuestId;

    public void CompleteQuest()
    {
        Debug.Log("퀘스트 완료");
        QuestManager.CompleteQuest(QuestId);
    }
}
