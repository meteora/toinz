﻿using UnityEngine;
using System.Collections;

public class Component_AssignQuest : MonoBehaviour {
    
    public int QuestId;
    public EmoticonType[] Emoticons;

    public void AcceptQuest()
    {
        QuestManager.AcceptQuest(QuestId, Emoticons[0]); 
    }

    public bool IsAcceptQuest()
    {
        return QuestManager.IsAcceptQuest(QuestId);
    }
}
