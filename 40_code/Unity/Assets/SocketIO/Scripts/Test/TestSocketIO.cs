﻿#region License
/*
 * TestSocketIO.cs
 *
 * The MIT License
 *
 * Copyright (c) 2014 Fabio Panettieri
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#endregion

using System.Collections;
using UnityEngine;
using SocketIO;
using UnityEngine.UI;
using System.Collections.Generic;

public class TestSocketIO : MonoBehaviour
{
	public SocketIOComponent socket;
    int count = 0;

    public bool isDone = false;

    public GameObject go_friendPrefab;

    public void Start() 
	{
        Debug.LogError("START");
		socket.On("open", TestOpen);
		socket.On("login", clogin);
        socket.On("cmove", cmove);
        socket.On("error", TestError);
		socket.On("close", TestClose);
        socket.On("tdisconnect", tdisconnect); 
    }


    public void Update()
    {
        
        //StartCoroutine("movecc");
    }

    private IEnumerator movecc()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["deviceUniqueIdentifier"] = SystemInfo.deviceUniqueIdentifier;

        do
        {
            data["x"] = VR_PlayerController.instance.go_myCharacter.transform.position.x.ToString();
            data["y"] = VR_PlayerController.instance.go_myCharacter.transform.position.z.ToString();

            yield return new WaitForSeconds(0.3f);
            socket.Emit("move", new JSONObject(data));
        } while (VR_PlayerController.instance.enu_state != PlayerState.MyHome);
    }

    public void cmove(SocketIOEvent e)    
    {
        Debug.Log("[SocketIO] Boop received: " + e.name + " " + e.data);

        if (e.data == null) { return; }

        string id = e.data.GetField("id").str;

        Vector3 v3_loc = Vector3.zero;
        v3_loc.x = float.Parse(e.data.GetField("x").str);
        v3_loc.z = float.Parse(e.data.GetField("y").str);

        if (VR_PlayerController.instance.enu_state == PlayerState.MyHome)
            return;

        if(!VR_PlayerController.instance.dict_friendsData.ContainsKey(id))
        {
            GameObject go_friend = Instantiate(go_friendPrefab);

            NetworkFriend scr_nfriend = go_friend.AddComponent<NetworkFriend>();
            if (scr_nfriend != null)
                VR_PlayerController.instance.dict_friendsData.Add(id, scr_nfriend);
        }

        if (VR_PlayerController.instance.dict_friendsData.ContainsKey(id))
            VR_PlayerController.instance.dict_friendsData[id].Location(v3_loc);
    }

	public void TestOpen(SocketIOEvent e)
	{
        
        if(socket.sid == null)
        {
            isDone = true;
            Debug.Log("[SocketIO] Open received: " + e.name + " " + e.data);
        }
        else
            Debug.Log("updated socket id " + socket.sid);
    }
	
	public void clogin(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Boop received: " + e.name + " " + e.data);

        if (e.data == null) { return; }
    }
	
	public void TestError(SocketIOEvent e)
	{
		//Debug.Log("[SocketIO] Error received: " + e.name + " " + e.data);
	}
	
	public void TestClose(SocketIOEvent e)
	{	
		Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
	}

    public void tdisconnect(SocketIOEvent e)
    {
        Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);

        string id = e.data.GetField("id").str;

        if(VR_PlayerController.instance.dict_friendsData.ContainsKey(id))
        {
            GameObject go_nfriend = VR_PlayerController.instance.dict_friendsData[id].gameObject;
            Destroy(go_nfriend);

            VR_PlayerController.instance.dict_friendsData.Remove(id);
        }
    }

    public void TownIn()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["deviceUniqueIdentifier"] = SystemInfo.deviceUniqueIdentifier;

        socket.Emit("login", new JSONObject(data));
    }

    public void TownOut()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["deviceUniqueIdentifier"] = SystemInfo.deviceUniqueIdentifier;

        socket.Emit("close", new JSONObject(data));
    }

    public void NetworkMoving()
    {
        StartCoroutine(movecc());
    }
}

