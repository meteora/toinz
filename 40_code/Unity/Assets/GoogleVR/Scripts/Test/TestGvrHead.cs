﻿using UnityEngine;
using System.Collections;

public class TestGvrHead : GvrHead {

    // Compute new head pose.
    protected override void UpdateHead()
    {
        if (updated)
        {  // Only one update per frame, please.
            return;
        }
        updated = true;
        GvrViewer.Instance.UpdateState();

        if (trackRotation)
        {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            var rot = transform.localRotation.eulerAngles; //Quaternion.identity;//transform.Rotate(Vector3.up, h );

            if (target == null)
            {

                //transform.localRotation = rot;
                rot.y += h;
                rot.x -= v;
                rot.z = 0f;
                transform.localRotation = Quaternion.Euler(rot);
                //transform.Rotate(Vector3.up, h);
                //transform.Rotate(Vector3.left, v);
            }
            else
            {
                rot.y += h;
                rot.x -= v;
                rot.z = 0f;
                transform.rotation = target.rotation * Quaternion.Euler(rot);//rot;
            }
        }

        if (trackPosition)
        {
            Vector3 pos = GvrViewer.Instance.HeadPose.Position;
            if (target == null)
            {
                transform.localPosition = pos;
            }
            else
            {
                transform.position = target.position + target.rotation * pos;
            }
        }

        OnHeadUpdate();
    }
}
